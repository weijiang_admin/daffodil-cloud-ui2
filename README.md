## Daffodil-cloud（达佛迪尔）基础管理平台微服务版`免费开源`
一个使用Java语言编程的代码简洁，通俗易读的微服务框架的基础项目脚手架。使用Maven搭建的SpringCloud工程项目，支持多数据源动态切换，前端使用vue3渐进式框架搭建的漂亮大方的管理平台界面。 

本人出生地水仙花之都【[漳州](http://www.zhangzhou.gov.cn/)】

Daffodil英文翻译意思是水仙花，本人也比较佛系且Daffodil音含 [ 达佛 ] 于是就叫“达佛迪尔基础管理平台”

#### 介绍
本系统是基于SpringCloud微服务的基础后台管理系统 代码简洁、易读易懂、界面美观、支持多数据源动态切换配置。采用技术与框架包括不限于SpringCloud(2021.0.1)、SpingBoot(2.6.4)、阿里Nacos(2.0.4)、缓存Redis（6.2+）、数据库Mysql(5.7+或8.0+）)、前端Vue3、ElementUI-Plus^2.2.9等等。

#### 演示地址
- [达佛迪尔基础管理平台演示示例（demo/123456）](http://www.daffodilcloud.com.cn)

系统基础包括：
- 基础管理：系统首页、个人中心、主题皮肤
- 系统管理：用户管理、角色管理(权限管理)、部门管理、岗位管理、职级管理、群组管理、通知管理
- 数据管理：菜单管理、字典管理、标签管理、区划管理、缓存管理、敏感词管理、应用管理、文件管理、日期管理、资源管理
- 审计管理：操作日志、登录日志、在线用户
- 系统设置：登录设置
- 监控管理：缓存监控、服务监控
- 开发管理：元数据管理、库表管理（代码生成）

#### 基础模块
- [daffodil-cloud-utils](https://gitee.com/weijiang_admin/daffodil-cloud/tree/master/daffodil-cloud-utils)：常用工具代码库
- [daffodil-cloud-code](https://gitee.com/weijiang_admin/daffodil-cloud/tree/master/daffodil-cloud-code)：底层核心代码库
- [daffodil-cloud-common](https://gitee.com/weijiang_admin/daffodil-cloud/tree/master/daffodil-cloud-common)：基础公共代码库
- [daffodil-cloud-framework](https://gitee.com/weijiang_admin/daffodil-cloud/tree/master/daffodil-cloud-framework)：基础框架代码库
- [daffodil-cloud-system](https://gitee.com/weijiang_admin/daffodil-cloud/tree/master/daffodil-cloud-system)：达佛基础平台微服务
- [daffodil-cloud-auth](https://gitee.com/weijiang_admin/daffodil-cloud/tree/master/daffodil-cloud-auth)：统一授权认证微服务
- [daffodil-cloud-gateway](https://gitee.com/weijiang_admin/daffodil-cloud/tree/master/daffodil-cloud-gateway)：网关路由微服务

#### 前端模块（分离单独仓库）
- [daffodil-cloud-ui](https://gitee.com/weijiang_admin/daffodil-cloud-ui)：达佛迪尔基础平台前端UI1.0.0工程（该前端使用的element-plus版本是^1.1.0-beta.9已停止更新维护）
- [daffodil-cloud-ui2](https://gitee.com/weijiang_admin/daffodil-cloud-ui2)：达佛迪尔基础平台前端UI2.0.0工程（该前端使用的element-plus版本是^2.2.9持续更新）

#### 其它模块（分离单独仓库）
- [daffodil-cloud-devtool](https://gitee.com/weijiang_admin/daffodil-cloud-devtool)：辅助开发微服务
- [daffodil-cloud-flowable](https://gitee.com/weijiang_admin/daffodil-cloud-flowable)：流程引擎微服务
- [daffodil-cloud-sensitive](https://gitee.com/weijiang_admin/daffodil-cloud-sensitive)：敏感词词语微服务（不文明用语）
- [daffodil-cloud-monitor](https://gitee.com/weijiang_admin/daffodil-cloud-monitor)：健康监控微服务
- [daffodil-cloud-cms](https://gitee.com/weijiang_admin/daffodil-cloud-cms)：简易内容管理微服务
- [daffodil-cloud-demo](https://gitee.com/weijiang_admin/daffodil-cloud-demo)：示例应用微服务
- [daffodil-cloud-medium](https://gitee.com/weijiang_admin/daffodil-cloud-medium)：讯息发送微服务
- [daffodil-cloud-storage](https://gitee.com/weijiang_admin/daffodil-cloud-storage)：对象存储微服务

#### 开发手册
- [达佛迪尔基础管理平台开发手册（coding）](https://daffodil.coding.net/s/9af19586-556c-4d13-ad18-de0a94d33aca)
- [达佛迪尔基础管理平台开发手册（gitee）](https://gitee.com/weijiang_admin/daffodil-cloud/wikis/pages)


#### 相关下载
达佛迪尔基础平台相关数据库脚本以及相关配置文件下载地址：[https://gitee.com/weijiang_admin/daffodil-cloud/tree/master/daffodil-cloud-doc](https://gitee.com/weijiang_admin/daffodil-cloud/tree/master/daffodil-cloud-doc)

#### 微信公众号
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/%E8%BE%BE%E4%BD%9BDFD%E5%BE%AE%E4%BF%A1%E5%85%AC%E4%BC%97%E5%8F%B7%E4%BA%8C%E7%BB%B4%E7%A0%81.jpg)  
【达佛迪尔】微信公众号

#### 质量评分
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/QQ%E6%88%AA%E5%9B%BE20220512144248.png)

#### 系统风采（基于前端UI2.0.0工程截图）
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/1.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/2.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/3.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/4.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/5.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/6.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/7.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/8.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/9.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/10.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/11.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/12.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/13.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/14.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/15.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/16.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/17.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/18.png)
![输入图片说明](https://gitee.com/weijiang_admin/daffodil-cloud/raw/master/daffodil-cloud-doc/src/images/19.png)

#### 特别感谢
特别感谢：@lyt-top前端框架支持[https://gitee.com/lyt-top/vue-next-admin](https://gitee.com/lyt-top/vue-next-admin)

#### 码云特技

1.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
2.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
3.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
4.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
5.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
