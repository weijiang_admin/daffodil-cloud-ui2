import { defineStore } from 'pinia';
import { DecoratorInfoState } from './interface';

export const defaultDecoratorInfo = {
	terminal: 1,
	terminalContent: {
		width: 415,
		title: '',//网页标题
		bgColor: '',
		imageUrl: '',
		confirm: false,//删除不提示确认框
		colNum: 24,
		rowHeight: 1,
		mgl: 0,
		mgr: 0,
		mgt: 0,
		mgb: 0,
	},
	terminalComponent: [{
		title: '拖拽布局',
		icon: 'fa fa-th-large',
		type: 'DragLayout',
		id: '',
		i: '',
		x: 0,
		y: 0, 
		w: 24, 
		h: 200,
		pdl: 0,
		pdr: 0,
		pdt: 0,
		pdb: 0,
		bgColor: '',
		imageUrl: '',
		terminalComponent: [],
	}],
	activeComponent: {},
	componentsFormData: {}, //表单组件数据
}

/**
 * 定义画布终端缓存
 */
export const useDecoratorInfo = defineStore('decoratorInfo', {
	state: (): DecoratorInfoState => (JSON.parse(JSON.stringify(defaultDecoratorInfo))),
	actions: {
		async setDecoratorInfo(decoratorInfo: any) {
			this.terminal = decoratorInfo.terminal;
			this.terminalContent = decoratorInfo.terminalContent;
			this.terminalComponent = decoratorInfo.terminalComponent;
			this.activeComponent = decoratorInfo.activeComponent;
			this.componentsFormData = decoratorInfo.componentsFormData;
		},
		async setTerminal(terminal: number) {
			this.terminal = terminal;
		},
		async setTerminalContent(terminalContent: object) {
			this.terminalContent = terminalContent;
		},
		async setTerminalComponent(terminalComponent: Array<object>) {
			this.setTerminalComponentSafety(terminalComponent);
		},
		async delActiveComponent(activeComponent: object) {
			this.setActiveComponent({});
			this.delActioveComponentSafety(activeComponent);
			this.setTerminalComponent(this.terminalComponent);
		},
		async delActioveComponentSafety(activeComponent: object) {
			if('DragLayout' === activeComponent.type){
				//第一层
				let i = this.terminalComponent?.indexOf(activeComponent);
				i >= 0 && this.terminalComponent.splice(i, 1);
				return;
			}else{
				//第二层
				for (let i = 0; i < this.terminalComponent?.length; i++) {
					let j = this.terminalComponent[i].terminalComponent?.indexOf(activeComponent);
					if(j >= 0){
						this.terminalComponent[i].terminalComponent.splice(j, 1);
						return;
					}
				}
				//第三层
				for (let i = 0; i < this.terminalComponent?.length; i++) {
					for (let j = 0; j < this.terminalComponent[i].terminalComponent?.length; j++) {
						let k = this.terminalComponent[i].terminalComponent[j].terminalComponent?.indexOf(activeComponent);
						if(k >= 0){
							this.terminalComponent[i].terminalComponent[j].terminalComponent.splice(k, 1);
							return;
						}
					}
				}
			}
		},
		async setTerminalComponentSafety(terminalComponent: Array<object>) {
			terminalComponent && terminalComponent.sort((a, b) => a.y === b.y ? a.x - b.x : a.y - b.y);
			this.terminalComponent = JSON.parse(JSON.stringify(terminalComponent));
		},
		async setActiveComponent(activeComponent: object) {
			this.activeComponent = activeComponent;
		},
		async setComponentsFormData(componentsFormData: object) {
			this.componentsFormData = componentsFormData;
		}
	},
});
