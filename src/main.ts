import { createApp } from 'vue';
import pinia from '/@/stores/index';
import App from './App.vue';
import router from './router';
import { directive } from '/@/utils/directive';
import { i18n } from '/@/i18n/index';
import other from '/@/utils/other';
import ElementPlus from 'element-plus';
import VueGridLayout from 'vue-grid-layout';
import zhCn from 'element-plus/es/locale/lang/zh-cn';
import mitt from 'mitt';

import 'element-plus/dist/index.css';
import 'font-awesome/css/font-awesome.min.css';
import '/@/theme/index.scss';

const app = createApp(App);

directive(app);
other.elSvg(app);

app.use(pinia).use(router).use(ElementPlus, { i18n: i18n.global.t, locale: zhCn }).use(i18n).use(VueGridLayout).mount('#app');

app.config.globalProperties.mittBus = mitt();