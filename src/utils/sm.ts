import { sm2, sm3 } from 'sm-crypto';
const cipherMode = 0;

export const SM = {
	//SM2加密
	sm2Encrypt : (data: string, publickey: string) => {
		if(data && publickey){
			const encryptData = sm2.doEncrypt(data, publickey, cipherMode);
			return '04' + encryptData;
		}
		return undefined;
	},
	//SM2解密
	sm2Decrypt : (data: string, privateKey: string) => {
		if(data && privateKey){
			let F = data.substring(0, 2);
			if(F != '04'){
				return undefined;
			}
			return sm2.doDecrypt(data.substring(2), privateKey, cipherMode);
		}
		return undefined;
	},
	sm3Encrypt : (data: string) => {
		if(data){
			return sm3(data);
		}
	    return undefined;
	}
}