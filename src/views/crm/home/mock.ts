import * as echarts from 'echarts';
/**
 * 最顶部 card
 * @returns 返回模拟数据
 */
export const topCardItemList = [
	{
		title: '用户登录总数',
		titleNum: '0',
		tip: '失败总数',
		tipNum: '0',
		color: '#F95959',
		iconColor: '#F86C6B',
		icon: 'iconfont icon-jinridaiban',
	},
	{
		title: '用户操作总数',
		titleNum: '0',
		tip: '失败总数',
		tipNum: '0',
		color: '#8595F4',
		iconColor: '#92A1F4',
		icon: 'iconfont icon-AIshiyanshi',
	},
	{
		title: '当前季度',
		titleNum: '1',
		tip: '当前期数',
		tipNum: '1',
		color: '#FEBB50',
		iconColor: '#FDC566',
		icon: 'iconfont icon-shenqingkaiban',
	},
];

/**
 * @returns 返回模拟数据
 */
export const contractCountInfoOption = {
	grid: {
		top: 30,
		right: 10,
		bottom: 30,
		left: 30,
	},
	tooltip: {
		trigger: 'axis',
	},
	legend: {
		data: ['总数'],
		right: 0,
	},
	color: [
		'#63caff',
		'#49beff',
		'#03387a',
		'#03387a',
		'#03387a',
		'#6c93ee',
		'#a9abff',
		'#f7a23f',
		'#27bae7',
		'#ff6d9d',
		'#cb79ff',
		'#f95b5a',
		'#ccaf27',
		'#38b99c',
		'#93d0ff',
		'#bd74e0',
		'#fd77da',
		'#dea700',
	],
	xAxis: {
		data: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
	},
	yAxis: [
		{
			type: 'value',
			name: '总数',
		},
	],
	series: [
		{
			name: '总数',
			type: 'bar',
			data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			itemStyle: {
				barBorderRadius: [4, 4, 0, 0],
				color: {
					x: 0,
					y: 0,
					x2: 0,
					y2: 1,
					type: 'linear',
					global: false,
					colorStops: [
						{
							offset: 0,
							color: '#0b9eff',
						},
						{
							offset: 1,
							color: '#63caff',
						},
					],
				},
			},
		}
	],
};

/**
 * @returns 返回模拟数据
 */
export const customerEnterpriseInfoOption = {
	grid: {
		top: 30,
		right: 10,
		bottom: 30,
		left: 30,
	},
	tooltip: {
		trigger: 'axis',
	},
	legend: {
		data: ['客户总数', '企业总数'],
		right: 0,
	},
	color: [
		'#63caff',
		'#49beff',
		'#03387a',
		'#03387a',
		'#03387a',
		'#6c93ee',
		'#a9abff',
		'#f7a23f',
		'#27bae7',
		'#ff6d9d',
		'#cb79ff',
		'#f95b5a',
		'#ccaf27',
		'#38b99c',
		'#93d0ff',
		'#bd74e0',
		'#fd77da',
		'#dea700',
	],
	xAxis: {
		data: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
	},
	yAxis: [
		{
			type: 'value',
			name: '总数',
		},
	],
	series: [
		{
			name: '客户总数',
			type: 'line',
			symbolSize: 6,
			symbol: 'circle',
			smooth: true,
			data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			lineStyle: { color: '#fe9a8b' },
			itemStyle: { color: '#fe9a8b', borderColor: '#fe9a8b' },
			areaStyle: {
				color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
					{ offset: 0, color: '#fe9a8bb3' },
					{ offset: 1, color: '#fe9a8b03' },
				]),
			},
		},
		{
			name: '企业总数',
			type: 'line',
			symbolSize: 6,
			symbol: 'circle',
			smooth: true,
			data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			lineStyle: { color: '#9E87FF' },
			itemStyle: { color: '#9E87FF', borderColor: '#9E87FF' },
			areaStyle: {
				color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
					{ offset: 0, color: '#9E87FFb3' },
					{ offset: 1, color: '#9E87FF03' },
				]),
			},
			emphasis: {
				itemStyle: {
					color: {
						type: 'radial',
						x: 0.5,
						y: 0.5,
						r: 0.5,
						colorStops: [
							{ offset: 0, color: '#9E87FF' },
							{ offset: 0.4, color: '#9E87FF' },
							{ offset: 0.5, color: '#fff' },
							{ offset: 0.7, color: '#fff' },
							{ offset: 0.8, color: '#fff' },
							{ offset: 1, color: '#fff' },
						],
					},
					borderColor: '#9E87FF',
					borderWidth: 2,
				},
			}
		},
	],
};


/**
 * @returns 返回模拟数据
 */
export const projectStatusOption = {
	tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
    },
	legend: {
        type: 'scroll',
        orient: 'vertical',
        right: 10,
        top: 20,
        bottom: 20,
        data: ['未开始','进行中','已暂停','已完成'],
    },
    series : [
        {
            name: '项目完成情况',
            type: 'pie',
            radius: '55%',
            // roseType: 'angle',
            data:[
                {value:0, name:'未开始'},
                {value:0, name:'进行中'},
                {value:0, name:'已暂停'},
                {value:0, name:'已完成'}
            ]
        }
    ]
};

/**
 * 登录浏览器信息
 * @returns 返回模拟数据
 */
export const businessLabelOption = {
    series : [
        {
            name: '操作类型',
            type: 'pie',
            radius: '55%',
            roseType: 'angle',
            data:[
                {value:1, name:'INSERT'},
                {value:1, name:'UPDATE'},
                {value:1, name:'DELETE'},
                {value:1, name:'OTHOR'}
            ]
        }
    ]
};

/**
 * @returns 返回模拟数据
 */
export const monthAmountInfoOption = {
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        },
        formatter: function (params) {
            var result = params[0].name + '<br/>';
            for (var i = 0; i < params.length; i++) {
                var sign = params[i].value >= 0 ? '+' : '';
                result += params[i].seriesName + ' : ' + sign + params[i].value + ' 元</br>';
            }
            return result;
        }
    },
	legend: {
		data: ['支出金额', '回款金额', '签约金额'],
		right: 0,
	},
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: {
        type: 'category',
        data: ['2024-08', '2024-09', '2024-10'] // 假设只有三个月的数据
    },
    yAxis: {
        type: 'value'
    },
    series: [
        {
            name: '支出金额',
            type: 'bar',
            data: [-20000, -15000, -10000] // 示例数据
        },
        {
            name: '回款金额',
            type: 'bar',
            data: [10000, 25000, 30000] // 示例数据
        },
        {
            name: '签约金额',
            type: 'bar',
            data: [5000, 10000, 15000] // 示例数据
        }
    ]
};
