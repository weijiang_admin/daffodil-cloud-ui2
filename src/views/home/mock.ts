import * as echarts from 'echarts';
/**
 * 最顶部 card
 * @returns 返回模拟数据
 */
export const topCardItemList = [
	{
		title: '用户登录总数',
		titleNum: '0',
		tip: '失败总数',
		tipNum: '0',
		color: '#F95959',
		iconColor: '#F86C6B',
		icon: 'iconfont icon-jinridaiban',
	},
	{
		title: '用户操作总数',
		titleNum: '0',
		tip: '失败总数',
		tipNum: '0',
		color: '#8595F4',
		iconColor: '#92A1F4',
		icon: 'iconfont icon-AIshiyanshi',
	},
	{
		title: '当前季度',
		titleNum: '1',
		tip: '当前期数',
		tipNum: '1',
		color: '#FEBB50',
		iconColor: '#FDC566',
		icon: 'iconfont icon-shenqingkaiban',
	},
];

/**
 * 登录信息
 * @returns 返回模拟数据
 */
export const loginInfoOption = {
	grid: {
		top: 30,
		right: 10,
		bottom: 30,
		left: 30,
	},
	tooltip: {
		trigger: 'axis',
	},
	legend: {
		data: ['登录总数', '失败总数'],
		right: 0,
	},
	color: [
		'#63caff',
		'#49beff',
		'#03387a',
		'#03387a',
		'#03387a',
		'#6c93ee',
		'#a9abff',
		'#f7a23f',
		'#27bae7',
		'#ff6d9d',
		'#cb79ff',
		'#f95b5a',
		'#ccaf27',
		'#38b99c',
		'#93d0ff',
		'#bd74e0',
		'#fd77da',
		'#dea700',
	],
	xAxis: {
		data: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
	},
	yAxis: [
		{
			type: 'value',
			name: '总数',
		},
	],
	series: [
		{
			name: '登录总数',
			type: 'bar',
			data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			itemStyle: {
				barBorderRadius: [4, 4, 0, 0],
				color: {
					x: 0,
					y: 0,
					x2: 0,
					y2: 1,
					type: 'linear',
					global: false,
					colorStops: [
						{
							offset: 0,
							color: '#0b9eff',
						},
						{
							offset: 1,
							color: '#63caff',
						},
					],
				},
			},
		},
		{
			name: '失败总数',
			type: 'line',
			data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			itemStyle: {
				color: '#febb50',
			},
		},
	],
};

/**
 * 操作信息
 * @returns 返回模拟数据
 */
export const operLogOption = {
	grid: {
		top: 30,
		right: 10,
		bottom: 30,
		left: 30,
	},
	tooltip: {
		trigger: 'axis',
	},
	legend: {
		data: ['操作总数', '失败总数'],
		right: 0,
	},
	color: [
		'#63caff',
		'#49beff',
		'#03387a',
		'#03387a',
		'#03387a',
		'#6c93ee',
		'#a9abff',
		'#f7a23f',
		'#27bae7',
		'#ff6d9d',
		'#cb79ff',
		'#f95b5a',
		'#ccaf27',
		'#38b99c',
		'#93d0ff',
		'#bd74e0',
		'#fd77da',
		'#dea700',
	],
	xAxis: {
		data: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
	},
	yAxis: [
		{
			type: 'value',
			name: '总数',
		},
	],
	series: [
		{
			name: '操作总数',
			type: 'line',
			symbolSize: 6,
			symbol: 'circle',
			smooth: true,
			data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			lineStyle: { color: '#fe9a8b' },
			itemStyle: { color: '#fe9a8b', borderColor: '#fe9a8b' },
			areaStyle: {
				color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
					{ offset: 0, color: '#fe9a8bb3' },
					{ offset: 1, color: '#fe9a8b03' },
				]),
			},
		},
		{
			name: '失败总数',
			type: 'line',
			symbolSize: 6,
			symbol: 'circle',
			smooth: true,
			data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			lineStyle: { color: '#9E87FF' },
			itemStyle: { color: '#9E87FF', borderColor: '#9E87FF' },
			areaStyle: {
				color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
					{ offset: 0, color: '#9E87FFb3' },
					{ offset: 1, color: '#9E87FF03' },
				]),
			},
			emphasis: {
				itemStyle: {
					color: {
						type: 'radial',
						x: 0.5,
						y: 0.5,
						r: 0.5,
						colorStops: [
							{ offset: 0, color: '#9E87FF' },
							{ offset: 0.4, color: '#9E87FF' },
							{ offset: 0.5, color: '#fff' },
							{ offset: 0.7, color: '#fff' },
							{ offset: 0.8, color: '#fff' },
							{ offset: 1, color: '#fff' },
						],
					},
					borderColor: '#9E87FF',
					borderWidth: 2,
				},
			}
		},
	],
};


/**
 * 登录浏览器信息
 * @returns 返回模拟数据
 */
export const browserOption = {
    series : [
        {
            name: '访问来源',
            type: 'pie',
            radius: '55%',
            roseType: 'angle',
            data:[
                {value:1, name:'Chrome 8'},
                {value:1, name:'Chrome 9'},
                {value:1, name:'Firefox 9'},
                {value:1, name:'Unknown'}
            ]
        }
    ]
};

/**
 * 登录浏览器信息
 * @returns 返回模拟数据
 */
export const businessLabelOption = {
    series : [
        {
            name: '操作类型',
            type: 'pie',
            radius: '55%',
            roseType: 'angle',
            data:[
                {value:1, name:'INSERT'},
                {value:1, name:'UPDATE'},
                {value:1, name:'DELETE'},
                {value:1, name:'OTHOR'}
            ]
        }
    ]
};
