export const indexTemplate: String = `<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="color-scheme" content="light dark">
		<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0,viewport-fit=cover">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="format-detection" content="telephone=no">
		<link rel="icon" href="/favicon.ico" />
		<title>\${site.name}</title>
	</head>
	<body>
		<h2>\${site.name}</h2>
	</body>
</html>`;
/**
 * 文档型模板
 */
export const documentTemplate: String = `<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="color-scheme" content="light dark">
		<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0,viewport-fit=cover">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="format-detection" content="telephone=no">
		<link rel="icon" href="/favicon.ico" />
		<title>\${document.title}</title>
		<style type="text/css">
			.media-page{max-width:677px;margin:0 auto;font-family:'Helvetica Neue', Helvetica, 'PingFang SC', 'Hiragino Sans GB', 'Microsoft YaHei', '微软雅黑', Arial, sans-serif;}
			.media-title{font-size:22px;font-weight:400;line-height:1.4;margin-bottom:14px;}
			.media-meta{font-size:15px;margin-bottom:22px;line-height:20px;word-wrap:break-word;}
			.media-meta a{margin:0;text-decoration:none;color: #576b95;}
			.media-meta span{margin-left:10px;color:#717375}
			.media-content{position:relative;visibility:visible;}
		</style>
	</head>
	<body>
		<div class="media-page">
			<h1 class="media-title">
				\${document.title}
			</h1>
			<div class="media-meta">
				<a href="javascript:void(0)">\${site.name} · \${channel.name}</a>
				<span>\${date}</span>
			</div>
			<div class="media-content">
				\${document.htmlContent}
			</div>
		</div>
	</body>
</html>`;

