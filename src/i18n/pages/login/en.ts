// 定义内容
export default {
	label: {
		account: 'Account password login',
		mobile: 'Verification code login',
	},
	account: {
		placeholder1: 'Please enter your tenant number',
		placeholder2: 'Please enter your user account or phone number or email account',
		placeholder3: 'Please enter your login password',
		placeholder4: 'Please enter the verification code',
		accountBtnText: 'Sign in',
	},
	mobile: {
		placeholder1: 'Please enter your tenant number',
		placeholder2: 'Please input mobile phone number or email account',
		placeholder3: 'Please enter the verification code',
		codeText: 'Get code',
		btnText: 'Sign in',
	},
	scan: {
		text: 'Open the mobile phone to scan and quickly log in / register',
		mode: 'Other login methods',
	},
	signInText: 'welcome back!',
	copyright: {
		company: 'Daffodil-cloud',
		recordno: 'Copyright: Daffodil-cloud fujian ICP preparation no.2023006511',
	},
	other: {
		forget: 'Forget password',
		tips: 'Warm tips: it is recommended to use browsers with edge ≥ 79, Firefox ≥ 78, chrome ≥ 64, Safari ≥ 12 and above, and 360 browser should use extreme speed mode',
	}
};
