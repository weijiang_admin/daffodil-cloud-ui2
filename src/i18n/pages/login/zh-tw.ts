// 定义内容
export default {
	label: {
		account: '賬號密碼登入',
		mobile: '驗證碼登入',
	},
	account: {
		placeholder1: '請輸入租戶號碼',
		placeholder2: '請輸入登錄賬號/手機號碼/郵箱賬號',
		placeholder3: '請輸入登錄密碼',
		placeholder4: '請輸入驗證碼',
		accountBtnText: '登 入',
	},
	mobile: {
		placeholder1: '請輸入租戶號碼',
		placeholder2: '請輸入手機號碼/郵箱賬號',
		placeholder3: '請輸入驗證碼',
		codeText: '獲取驗證碼',
		btnText: '登入',
		msgText: '* 溫馨提示：建議使用Edge ≥ 79、Firefox ≥ 78、Chrome ≥ 64、Safari ≥ 12及以上的浏覽器，360浏覽器請使用極速模式',
	},
	scan: {
		text: '打開手機掃一掃，快速登錄/注册',
		mode: '其他登錄方式',
	},
	signInText: '歡迎回來！',
	copyright: {
		company: '達佛迪爾',
		recordno: '版權所有:  達佛迪爾 閩ICP備2023006511號',
	},
	other: {
		forget: '忘記密碼',
		tips: '* 溫馨提示：建議使用Edge ≥ 79、Firefox ≥ 78、Chrome ≥ 64、Safari ≥ 12及以上的浏覽器，360浏覽器請使用極速模式',
	}
};
