// 定义内容
export default {
	label: {
		account: '账号密码登录',
		mobile: '验证码登录',
	},
	account: {
		placeholder1: '请输入租户号码',
		placeholder2: '请输入登录账号/手机号码/邮箱账号',
		placeholder3: '请输入登录密码',
		placeholder4: '请输入验证码',
		accountBtnText: '登 录',
	},
	mobile: {
		placeholder1: '请输入租户号码',
		placeholder2: '请输入手机号码/邮箱账号',
		placeholder3: '请输入验证码',
		codeText: '获取验证码',
		btnText: '登 录',
	},
	scan: {
		text: '打开手机扫一扫，快速登录/注册',
		mode: '其他登录方式',
	},
	signInText: '欢迎回来！',
	copyright: {
		company: '达佛迪尔',
		recordno: '版权所有: 达佛迪尔 闽ICP备2023006511号',
	},
	other: {
		forget: '忘记密码',
		tips: '* 温馨提示：建议使用Edge ≥ 79、Firefox ≥ 78、Chrome ≥ 64、Safari ≥ 12及以上的浏览器，360浏览器请使用极速模式',
	}
};
