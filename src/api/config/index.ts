import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 获取系统应用设置详情
 * @param params 
 */
export function getConfig(params?: object){
	return request({
		url: system_api + '/config/info',
		method: 'get',
		params: params
	})
}

/** 修改系统应用设置 */
export function editConfig(token: string, params?: object) {
	return request({
		url: system_api + '/config/edit',
		method: 'post',
		headers: {
			'X-Request-Second-Verify-Token': token,
		},
		data: params
	});
}

