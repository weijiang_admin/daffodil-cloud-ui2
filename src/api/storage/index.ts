import request from '/@/utils/request';

const storage_api = import.meta.env.VITE_STORAGE_API as string;

/**
 * 获取对象管理列表
 * @param params 
 */
export function getStorageList(params?: object){
	return request({
		url: storage_api + '/list',
		method: 'get',
		params: params
	})
}

/**
 * 获取对象信息
 * @param params 
 */
export function getStorage(params?: object){
	return request({
		url: storage_api + '/info',
		method: 'get',
		params: params
	})
}

/** 上传对象
 * @param key 文件夹名称
 * @param params source 远端网络地址
 */
export function uploadStorage(params?: object){
	// return request({
	// 	url: storage_api + '/upload',
	// 	method: 'post',
	// 	data: params
	// });
	return storage_api + '/upload';
}

/**
 * 下载对象
 */
export function downloadStorage(params?: object){
	return request({
		url: storage_api + '/download',
		method: 'get',
		params: params
	})
}

/** 删除对象 */
export function delStorage(params?: object){
	return request({
		url: storage_api + '/remove',
		method: 'post',
		data: params
	});
}