import request from '/@/utils/request';

const devtool_api = '/api-devtool';

/**
 * 获取库表字段管理列表
 * @param params 
 */
export function getGencolumnList(params?: object){
	return request({
		url: devtool_api + '/gencolumn/list',
		method: 'get',
		params: params
	})
}

/** 新增库表字段 */
export function addGencolumn(params?: object){
	return request({
		url: devtool_api + '/gencolumn/add',
		method: 'post',
		data: params
	});
}

