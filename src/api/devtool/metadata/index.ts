import request from '/@/utils/request';

const devtool_api = '/api-devtool';

/**
 * 获取元数据管理列表
 * @param params 
 */
export function getMetadataList(params?: object){
	return request({
		url: devtool_api + '/metadata/list',
		method: 'get',
		params: params
	})
}

/**
 * 获取元数据信息
 * @param params 
 */
export function getMetadata(params?: object){
	return request({
		url: devtool_api + '/metadata/info',
		method: 'get',
		params: params
	})
}

/** 新增元数据 */
export function addMetadata(params?: object){
	return request({
		url: devtool_api + '/metadata/add',
		method: 'post',
		data: params
	});
}

/** 修改元数据 */
export function editMetadata(params?: object) {
	return request({
		url: devtool_api + '/metadata/edit',
		method: 'post',
		data: params
	});
}

/** 删除元数据 */
export function delMetadata(params?: object){
	return request({
		url: devtool_api + '/metadata/remove',
		method: 'post',
		data: params
	});
}
