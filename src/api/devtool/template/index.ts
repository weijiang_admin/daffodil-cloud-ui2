import request from '/@/utils/request';

const devtool_api = '/api-devtool';

/**
 * 获取生成代码模板管理列表
 * @param params 
 */
export function getGenTemplateList(params?: object){
    return request({
        url: devtool_api + '/template/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取生成代码模板
 * @param params 
 */
export function getGenTemplate(params?: object){
    return request({
        url: devtool_api + '/template/info',
        method: 'get',
        params: params
    })
}

/** 新增生成代码模板 */
export function addGenTemplate(params? : object){
    return request({
        url: devtool_api + '/template/add',
        method: 'post',
        data: params
    });
}

/** 修改生成代码模板 */
export function editGenTemplate(params?: object) {
    return request({
        url: devtool_api + '/template/edit',
        method: 'post',
        data: params
    });
}

/** 删除生成代码模板 */
export function delGenTemplate(params?: object){
    return request({
        url: devtool_api + '/template/remove',
        method: 'post',
        data: params
    });
}
