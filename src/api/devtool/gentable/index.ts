import request from '/@/utils/request';

const devtool_api = '/api-devtool';

/**
 * 获取数据库表管理列表
 * @param params 
 */
export function getGentableList(params?: object){
	return request({
		url: devtool_api + '/gentable/list',
		method: 'get',
		params: params
	})
}

/**
 * 获取数据库表信息
 * @param params 
 */
export function getGentable(params?: object){
	return request({
		url: devtool_api + '/gentable/info',
		method: 'get',
		params: params
	})
}

/** 新增数据库表 */
export function addGentable(params?: object){
	return request({
		url: devtool_api + '/gentable/add',
		method: 'post',
		data: params
	});
}

/** 修改数据库表 */
export function editGentable(params?: object) {
	return request({
		url: devtool_api + '/gentable/edit',
		method: 'post',
		data: params
	});
}

/** 删除数据库表 */
export function delGentable(params?: object){
	return request({
		url: devtool_api + '/gentable/remove',
		method: 'post',
		data: params
	});
}

/**
 * 预览生成代码信息
 * @param params 
 */
export function previewGencode(params?: object){
	return request({
		url: devtool_api + '/gencode/preview/' + params.id,
		method: 'get'
	})
}

/**
 * 下载生成代码信息
 * @param params 
 */
export function downloadGencode(params?: object){
	return request({
		url: devtool_api + '/gencode/download/' + params.id,
		method: 'get',
		responseType: "blob"
	})
}
