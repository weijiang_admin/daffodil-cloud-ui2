import request from '/@/utils/request';

const auth_api = import.meta.env.VITE_AUTH_API as string;

/**
 * 用户登录
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function signIn(params?: object) {
	return request({
		url: auth_api + '/token/sign',
		method: 'post',
		data: params
	});
}

/**
 * 用户退出登录
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function signOut(params?: object) {
	return request({
		url: auth_api + '/token/destroy',
		method: 'post',
		data: params
	});
}

/**
 * 续期token
 * @param params {refreshToken: refreshToken}
 */
export function tokenRenewal(params?: any){
	return request({
		url: auth_api + '/token/renewal',
		method: 'get',
		headers: {
			'X-Request-Refresh-Token': params.refreshToken,
		}
	})
}

/**
 * 刷新token
 * @param params {refreshToken: refreshToken}
 */
export function tokenRefresh(params?: any){
	return request({
		url: auth_api + '/token/refresh',
		method: 'get',
		headers: {
			'X-Request-Refresh-Token': params.refreshToken,
		}
	})
}

/**
 * 获取平台预备数据(包括sm2公钥)
 * @param params 获取平台预备数据
 * @returns 返回接口数据
 */
export function getPlatformPrepar(params?: object){
	return request({
		url: auth_api + '/platform/prepar',
		method: 'get',
		params: params
	})
}

/**
 * 用户密码验证
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function pwdVerify(params?: object) {
	return request({
		url: auth_api + '/password/verify',
		method: 'post',
		data: params
	});
}

/**
 * 获取登录用户信息
 * @param params 
 * @returns 返回接口数据
 */
export function getLoginUserInfo(params?: object){
	return request({
		url: auth_api + '/user/info',
		method: 'get',
		params: params
	})
}

/**
 * 获取登录用户授权页面
 * @param params 
 * @returns 返回接口数据
 */
export function getLoginUserAuthPage(params?: object){
	return request({
		url: auth_api + '/user/auth/page',
		method: 'get',
		params: params
	})
}

/**
 * 获取登录用户授权操作
 * @param params 
 * @returns 返回接口数据
 */
export function getLoginUserAuthBtn(params?: object){
	return request({
		url: auth_api + '/user/auth/button',
		method: 'get',
		params: params
	})
}

/**
 * 获取登录用户路由菜单
 * @param params 
 */
export function getLoginUserRouterMenu(params?: object){
	return request({
		url: auth_api + '/user/router/menu',
		method: 'get',
		params: params
	});
}

/**
 * 获取验证码
 * @param params 
 * @returns 返回接口数据
 */
export function kaptchaVerify(params?: any){
	return request({
		url: auth_api + '/kaptcha/' + params.verifyKey,
		method: 'get',
		responseType: "blob"
	})
}

/**
 * 发送验证码
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function kaptchaSend(params?: object) {
	return request({
		url: auth_api + '/kaptcha/send',
		method: 'post',
		data: params
	});
}

/**
 * 获取登录设置
 * @param params 
 */
export function getConfigLogin(params?: object){
	return request({
		url: auth_api + '/config/login',
		method: 'get',
		params: params
	});
}

/**
 * 动态验证码验证(密码找回)
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function forgetPasswordVerify(params?: object) {
	return request({
		url: auth_api + '/forget/password/verify',
		method: 'post',
		data: params
	});
}

/**
 * 密码重置(密码找回)
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function forgetPasswordReset(params?: object) {
	return request({
		url: auth_api + '/forget/password/reset',
		method: 'post',
		data: params
	});
}

/**
 * 获取扫码登录模式配置
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function getScanAuthorize(params?: object) {
	return request({
		url: auth_api + '/scan/authorize',
		method: 'get',
		params: params
	});
}

/**
 * 用户扫码登录
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function scanLogin(params?: object) {
	return request({
		url: auth_api + '/scan/login',
		method: 'post',
		data: params
	});
}

/**
 * 用户扫码绑定
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function scanBind(params?: object) {
	return request({
		url: auth_api + '/scan/bind',
		method: 'post',
		data: params
	});
}

