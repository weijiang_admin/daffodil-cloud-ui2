import request from '/@/utils/request';

const flowable_api = import.meta.env.VITE_FLOWABLE_API as string;

/**
 * 获取待部署模板管理列表
 * @param params 
 */
export function getModelerUndeployList(params?: object){
	return request({
		url: flowable_api + '/modeler/list/undeploy',
		method: 'get',
		params: params
	})
}

/**
 * 获取已部署模板管理列表
 * @param params 
 */
export function getModelerDeployList(params?: object){
	return request({
		url: flowable_api + '/modeler/list/deploy',
		method: 'get',
		params: params
	})
}

/** 部署模板 */
export function deployModeler(params?: object){
	return request({
		url: `${flowable_api}/modeler/deploy/${params.modelId}/${params.category}`,
		method: 'post',
		data: params
	});
}

/** 撤销部署模板 */
export function undeployModeler(params?: object) {
	return request({
		url: `${flowable_api}/modeler/undeploy/${params.deployId}`,
		method: 'post',
		data: params
	});
}

/**
 * 获取模板预览地址
 * @param params
 */
export function getModelerDiagram(modelKey : string){
	return request({
		url: `${flowable_api}/modeler/diagram/${modelKey}`,
		method: 'get',
		responseType: "blob"
	});
}
