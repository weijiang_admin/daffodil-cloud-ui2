import request from '/@/utils/request';

const flowable_api = import.meta.env.VITE_FLOWABLE_API as string;

/**
 * 获取任务数据列表
 * @param params 
 */
export function getFlowableTaskMyselfList(params?: object){
	return request({
		url: `${flowable_api}/task/list/myself/${params.process}`,
		method: 'get',
		params: params
	})
}

/**
 * 获取任务数据列表
 * @param params 
 */
export function getFlowableTaskList(params?: object){
	return request({
		url: `${flowable_api}/task/list/${params.process}`,
		method: 'get',
		params: params
	})
}

/**
 * 获取已部署的流程信息
 * @param params deployId | modelKey
 */
export function getFlowModelerInfo(params?: object){
	return request({
		url: flowable_api + '/task/modeler/info',
		method: 'get',
		params: params
	})
}

/** 
 * 获取当前环节信息
 * @param businessKey
 */
export function getFlowCurrentNode(businessKey: string){
	return request({
		url: `${flowable_api}/task/current/node/${businessKey}`,
		method: 'get'
	});
}

/** 
 * 获取流程记录信息 
 * @param businessKey
 */
export function getFlowActivitys(businessKey: string) {
	return request({
		url: `${flowable_api}/task/activitys/${businessKey}`,
		method: 'get'
	});
}

/** 
 * 获取当前流程按钮信息
 * @param businessKey
 */
export function getFlowButtons(businessKey: string) {
	if(businessKey){
		return request({
			url: `${flowable_api}/task/buttons/${businessKey}`,
			method: 'get'
		});
	}else{
		return request({
			url: `${flowable_api}/task/buttons`,
			method: 'get'
		});
	}
}

/**
 * 获取流程图
 * @param businessKey
 */
export function getFlowDiagram(businessKey: string){
	return request({
		url: `${flowable_api}/task/diagram/${businessKey}`,
		method: 'get',
		responseType: "blob"
	});
}

/** 
 * 获取下一环节办理人信息
 * @param params businessKey / handleId / nodeId
 */
export function getFlowNextAssignee(params?: object) {
	return request({
		url: `${flowable_api}/task/next/assignee/${params.businessKey}/${params.handleId}/${params.nodeId}`,
		method: 'get'
	});
}

/** 
 * 获取选择下一步环节节点
 * @param params businessKey / handleId
 */
export function getFlowNextNodes(params?: object) {
	return request({
		url: `${flowable_api}/task/next/nodes/${params.businessKey}/${params.handleId}`,
		method: 'get'
	});
}

/** 
 * 获取选择下一步环节操作信息
 * @param businessKey
 */
export function getFlowNextHandles(businessKey: string) {
	return request({
		url: `${flowable_api}/task/next/handles/${businessKey}`,
		method: 'get'
	});
}

/** 
 * 流程初始化
 * @param params
 */
export function flowTaskStart(params?: object) {
	return request({
		url: flowable_api + '/task/start',
		method: 'post',
		data: params
	});
}

/** 
 * 流程办理
 * @param businessKey 业务ID
 * @param handleId	操作ID
 * @param message	办理意见
 * @param attachmentName	附件名称
 * @param attachmentDescription	附件描述
 * @param attachmentFile	附件文件
 * @param nodeIds	下一环节节点集合
 * @param userIds	下一环节办理用户集合
 */
export function flowTaskHandle(params?: object) {
	return request({
		url: flowable_api + '/task/handle',
		method: 'post',
		data: params
	});
}

/** 
 * 流程作废
 * @param params businessKey
 */
export function flowTaskTrash(params?: object) {
	return request({
		url: flowable_api + '/task/trash',
		method: 'post',
		data: params
	});
}

/** 
 * 流程撤办
 * @param params businessKey
 */
export function flowTaskBack(params?: object) {
	return request({
		url: flowable_api + '/task/back',
		method: 'post',
		data: params
	});
}

/** 
 * 流程驳回
 * @param params businessKey | message
 */
export function flowTaskReject(params?: object) {
	return request({
		url: flowable_api + '/task/reject',
		method: 'post',
		data: params
	});
}

