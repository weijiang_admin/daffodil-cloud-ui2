import router from '/@/router';

// 流程引擎callbackId回调方法集合
export const CALLBACK = {
	//流程演示（财务报销）
	expenseCallback: (row) => {
		router.push({
			path: '/flowable/demo/expense/view',
			query: { id : row.businessKey, t: new Date().getTime() }
		});
	}
}