import request from '/@/utils/request';

const demo_api = '/api-demo';

/**
 * 获取学生信息管理列表
 * @param params 
 */
export function getStudentList(params?: object){
    return request({
        url: demo_api + '/school/student/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取学生信息
 * @param params 
 */
export function getStudent(params?: object){
    return request({
        url: demo_api + '/school/student/info',
        method: 'get',
        params: params
    })
}

/** 新增学生信息 */
export function addStudent(params? : object){
    return request({
        url: demo_api + '/school/student/add',
        method: 'post',
        data: params
    });
}

/** 修改学生信息 */
export function editStudent(params?: object) {
    return request({
        url: demo_api + '/school/student/edit',
        method: 'post',
        data: params
    });
}

/** 删除学生信息 */
export function delStudent(params?: object){
    return request({
        url: demo_api + '/school/student/remove',
        method: 'post',
        data: params
    });
}
