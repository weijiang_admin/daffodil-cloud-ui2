import request from '/@/utils/request';

const auth_api = import.meta.env.VITE_AUTH_API as string;

/**
 * 获取第三方应用基本信息
 * @param params 
 */
export function getOauthInfo(params?: object){
	return request({
		url: auth_api + '/oauth/info',
		method: 'get',
		params: params
	})
}

/**
 * 获取第三方应用授权码
 * @param params 
 */
export function getOauthGrant(params?: object){
	return request({
		url: auth_api + '/oauth/grant',
		method: 'get',
		params: params
	})
}