import request from '/@/utils/request';

const fastdata_api = '/api-fastdata';

/**
 * 获取数据源管理列表
 * @param params 
 */
export function getDbsourceList(params?: object){
    return request({
        url: fastdata_api + '/dbsource/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取数据源
 * @param params 
 */
export function getDbsource(params?: object){
    return request({
        url: fastdata_api + '/dbsource/info',
        method: 'get',
        params: params
    })
}

/** 新增数据源 */
export function addDbsource(params? : object){
    return request({
        url: fastdata_api + '/dbsource/add',
        method: 'post',
        data: params
    });
}

/** 修改数据源 */
export function editDbsource(params?: object) {
    return request({
        url: fastdata_api + '/dbsource/edit',
        method: 'post',
        data: params
    });
}

/** 删除数据源 */
export function delDbsource(params?: object){
    return request({
        url: fastdata_api + '/dbsource/remove',
        method: 'post',
        data: params
    });
}
