import request from '/@/utils/request';

const fastdata_api = '/api-fastdata';

/**
 * 获取数据脚本管理列表
 * @param params 
 */
export function getDbscriptList(params?: object){
    return request({
        url: fastdata_api + '/dbscript/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取数据脚本
 * @param params 
 */
export function getDbscript(params?: object){
    return request({
        url: fastdata_api + '/dbscript/info',
        method: 'get',
        params: params
    })
}

/** 新增数据脚本 */
export function addDbscript(params? : object){
    return request({
        url: fastdata_api + '/dbscript/add',
        method: 'post',
        data: params
    });
}

/** 修改数据脚本 */
export function editDbscript(params?: object) {
    return request({
        url: fastdata_api + '/dbscript/edit',
        method: 'post',
        data: params
    });
}

/** 删除数据脚本 */
export function delDbscript(params?: object){
    return request({
        url: fastdata_api + '/dbscript/remove',
        method: 'post',
        data: params
    });
}
