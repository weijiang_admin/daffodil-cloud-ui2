import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/** 修改登录用户信息 */
export function editUserInfo(token: string, params?: object) {
	return request({
		url: system_api + '/user/info/edit',
		method: 'post',
		headers: {
			'X-Request-Second-Verify-Token': token,
		},
		data: params
	});
}

/** 触发更新用户消息中心 */
export function targetUserNotice(params?: object) {
	return request({
		url: system_api + '/user/notice/target',
		method: 'post',
		data: params
	});
}

/**
 * 获取用户消息中心列表
 * @param params 
 */
export function getUserNoticeList(params?: object){
	return request({
		url: system_api + '/user/notice/list',
		method: 'get',
		params: params
	})
}

/** 通知消息标为已读 */
export function readUserNotice(params?: object) {
	return request({
		url: system_api + '/user/notice/read',
		method: 'post',
		data: params
	});
}

/**
 * 分页查询用户绑定第三方账号列表
 * @param params 
 */
export function getSocialUserList(params?: object){
	return request({
		url: system_api + '/user/social/list',
		method: 'get',
		params: params
	})
}

/** 用户解绑第三方账号 */
export function freeSocialUser(params?: object) {
	return request({
		url: system_api + '/user/social/free',
		method: 'post',
		data: params
	});
}
