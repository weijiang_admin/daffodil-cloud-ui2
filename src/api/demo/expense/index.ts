import request from '/@/utils/request';

const demo_api = '/api-demo';

/**
 * 获取费用报销列表
 * @param params 
 */
export function getExpenseList(params?: object){
	return request({
		url: demo_api + '/expense/list',
		method: 'get',
		params: params
	})
}

/**
 * 获取费用报销
 * @param params 
 */
export function getExpense(params?: object){
	return request({
		url: demo_api + '/expense/info',
		method: 'get',
		params: params
	})
}

/** 新增费用报销 */
export function addExpense(params?: object){
	return request({
		url: demo_api + '/expense/add',
		method: 'post',
		data: params
	});
}

/** 修改费用报销 */
export function editExpense(params?: object) {
	return request({
		url: demo_api + '/expense/edit',
		method: 'post',
		data: params
	});
}

/** 删除费用报销 */
export function delExpense(params?: object){
	return request({
		url: demo_api + '/expense/remove',
		method: 'post',
		params: params
	});
}
