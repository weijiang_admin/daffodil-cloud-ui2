import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 获取数据源管理列表
 * @param params 
 */
export function getDatasourceList(params?: object){
    return request({
        url: system_api + '/datasource/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取数据源
 * @param params 
 */
export function getDatasource(params?: object){
    return request({
        url: system_api + '/datasource/info',
        method: 'get',
        params: params
    })
}

/** 新增数据源 */
export function addDatasource(params? : object){
    return request({
        url: system_api + '/datasource/add',
        method: 'post',
        data: params
    });
}

/** 修改数据源 */
export function editDatasource(params?: object) {
    return request({
        url: system_api + '/datasource/edit',
        method: 'post',
        data: params
    });
}

/** 删除数据源 */
export function delDatasource(params?: object){
    return request({
        url: system_api + '/datasource/remove',
        method: 'post',
        data: params
    });
}
