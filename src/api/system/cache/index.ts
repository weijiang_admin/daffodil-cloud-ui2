import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 查询Redis缓存键值列表
 * @param params 
 */
export function getRedisCacheKeys(params?: object){
	return request({
		url: system_api + '/rediscache/keys',
		method: 'get',
		params: params
	})
}

/**
 * 根据Redis缓存键值获取数据
 * @param params 
 */
export function getRedisCacheValues(params?: object){
	return request({
		url: system_api + '/rediscache/values',
		method: 'get',
		params: params
	})
}
