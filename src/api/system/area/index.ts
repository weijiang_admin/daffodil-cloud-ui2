import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 获取区划管理列表
 * @param params 
 */
export function getAreaList(params?: object){
	return request({
		url: system_api + '/area/list',
		method: 'get',
		params: params
	})
}

/**
 * 获取区划信息
 * @param params 
 */
export function getArea(params?: object){
	return request({
		url: system_api + '/area/info',
		method: 'get',
		params: params
	})
}

/** 新增区划 */
export function addArea(params?: object){
	return request({
		url: system_api + '/area/add',
		method: 'post',
		data: params
	});
}

/** 修改区划 */
export function editArea(params?: object) {
	return request({
		url: system_api + '/area/edit',
		method: 'post',
		data: params
	});
}

/** 删除区划 */
export function delArea(params?: object){
	return request({
		url: system_api + '/area/remove',
		method: 'post',
		data: params
	});
}
