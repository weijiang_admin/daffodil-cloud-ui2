import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 获取字典管理列表
 * @param params 
 */
export function getDictionaryList(params?: object){
	return request({
		url: system_api + '/dictionary/list',
		method: 'get',
		params: params
	})
}

/**
 * 获取字典信息
 * @param params 
 */
export function getDictionary(params?: object){
	return request({
		url: system_api + '/dictionary/info',
		method: 'get',
		params: params
	})
}

/** 新增字典 */
export function addDictionary(params?: object){
	return request({
		url: system_api + '/dictionary/add',
		method: 'post',
		data: params
	});
}

/** 修改字典 */
export function editDictionary(params?: object) {
	return request({
		url: system_api + '/dictionary/edit',
		method: 'post',
		data: params
	});
}

/** 删除字典 */
export function delDictionary(params?: object){
	return request({
		url: system_api + '/dictionary/remove',
		method: 'post',
		data: params
	});
}

/**
 * 获取字典信息
 * @param params {dictLabel: 'sys_yes_no'}
 */
export function getDictionaryByLabel(params?: object){
	return request({
		url: system_api + '/dictionary/label',
		method: 'get',
		params: params
	})
}
