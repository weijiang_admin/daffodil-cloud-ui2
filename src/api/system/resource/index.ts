import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 获取开放资源信息管理列表
 * @param params 
 */
export function getResourceList(params?: object){
    return request({
        url: system_api + '/resource/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取开放资源信息
 * @param params 
 */
export function getResource(params?: object){
    return request({
        url: system_api + '/resource/info',
        method: 'get',
        params: params
    })
}

/** 新增开放资源信息 */
export function addResource(params? : object){
    return request({
        url: system_api + '/resource/add',
        method: 'post',
        data: params
    });
}

/** 修改开放资源信息 */
export function editResource(params?: object) {
    return request({
        url: system_api + '/resource/edit',
        method: 'post',
        data: params
    });
}

/** 删除开放资源信息 */
export function delResource(params?: object){
    return request({
        url: system_api + '/resource/remove',
        method: 'post',
        data: params
    });
}
