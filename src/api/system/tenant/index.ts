import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 获取租户管理列表
 * @param params 
 */
export function getTenantList(params?: object){
    return request({
        url: system_api + '/tenant/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取租户
 * @param params 
 */
export function getTenant(params?: object){
    return request({
        url: system_api + '/tenant/info',
        method: 'get',
        params: params
    })
}

/** 新增租户 */
export function addTenant(params? : object){
    return request({
        url: system_api + '/tenant/add',
        method: 'post',
        data: params
    });
}

/** 修改租户 */
export function editTenant(params?: object) {
    return request({
        url: system_api + '/tenant/edit',
        method: 'post',
        data: params
    });
}

/** 删除租户 */
export function delTenant(params?: object){
    return request({
        url: system_api + '/tenant/remove',
        method: 'post',
        data: params
    });
}
