import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 获取菜单管理列表
 * @param params 
 */
export function getMenuList(params?: object){
	return request({
		url: system_api + '/menu/list',
		method: 'get',
		params: params
	})
}

/**
 * 获取菜单信息
 * @param params 
 */
export function getMenu(params?: object){
	return request({
		url: system_api + '/menu/info',
		method: 'get',
		params: params
	})
}

/** 新增菜单 */
export function addMenu(params?: object){
	return request({
		url: system_api + '/menu/add',
		method: 'post',
		data: params
	});
}

/** 修改菜单 */
export function editMenu(params?: object) {
	return request({
		url: system_api + '/menu/edit',
		method: 'post',
		data: params
	});
}

/** 删除菜单 */
export function delMenu(params?: object){
	return request({
		url: system_api + '/menu/remove',
		method: 'post',
		data: params
	});
}
