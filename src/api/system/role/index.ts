import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 获取角色管理列表
 * @param params 
 */
export function getRoleList(params?: object){
	return request({
		url: system_api + '/role/list',
		method: 'get',
		params: params
	})
}

/**
 * 获取角色信息
 * @param params 
 */
export function getRole(params?: object){
	return request({
		url: system_api + '/role/info',
		method: 'get',
		params: params
	})
}

/** 新增角色 */
export function addRole(params?: object){
	return request({
		url: system_api + '/role/add',
		method: 'post',
		data: params
	});
}

/** 修改角色 */
export function editRole(params?: object) {
	return request({
		url: system_api + '/role/edit',
		method: 'post',
		data: params
	});
}

/** 删除角色 */
export function delRole(params?: object){
	return request({
		url: system_api + '/role/remove',
		method: 'post',
		data: params
	});
}
