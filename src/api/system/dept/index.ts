import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 获取部门管理列表
 * @param params 
 */
export function getDeptList(params?: object){
	return request({
		url: system_api + '/dept/list',
		method: 'get',
		params: params
	})
}

/**
 * 获取部门信息
 * @param params 
 */
export function getDept(params?: object){
	return request({
		url: system_api + '/dept/info',
		method: 'get',
		params: params
	})
}

/** 新增部门 */
export function addDept(params?: object){
	return request({
		url: system_api + '/dept/add',
		method: 'post',
		data: params
	});
}

/** 修改部门 */
export function editDept(params?: object) {
	return request({
		url: system_api + '/dept/edit',
		method: 'post',
		data: params
	});
}

/** 删除部门 */
export function delDept(params?: object){
	return request({
		url: system_api + '/dept/remove',
		method: 'post',
		data: params
	});
}
