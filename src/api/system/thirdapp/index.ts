import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 获取第三方应用管理列表
 * @param params 
 */
export function getThirdAppList(params?: object){
    return request({
        url: system_api + '/thirdapp/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取第三方应用
 * @param params 
 */
export function getThirdApp(params?: object){
    return request({
        url: system_api + '/thirdapp/info',
        method: 'get',
        params: params
    })
}

/** 新增第三方应用 */
export function addThirdApp(params? : object){
    return request({
        url: system_api + '/thirdapp/add',
        method: 'post',
        data: params
    });
}

/** 修改第三方应用 */
export function editThirdApp(params?: object) {
    return request({
        url: system_api + '/thirdapp/edit',
        method: 'post',
        data: params
    });
}

/** 删除第三方应用 */
export function delThirdApp(params?: object){
    return request({
        url: system_api + '/thirdapp/remove',
        method: 'post',
        data: params
    });
}

/** 重置第三方应用秘钥 */
export function resetThirdApp(params?: object){
    return request({
        url: system_api + '/thirdapp/reset',
        method: 'post',
        data: params
    });
}

