import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 获取操作日志管理列表
 * @param params 
 */
export function getOperlogList(params?: object){
	return request({
		url: system_api + '/operlog/list',
		method: 'get',
		params: params
	});
}

/**
 * 获取操作日志信息
 * @param params 
 */
export function getOperlog(params?: object){
	return request({
		url: system_api + '/operlog/info',
		method: 'get',
		params: params
	});
}

/** 删除操作日志 */
export function delOperlog(params?: object){
	return request({
		url: system_api + '/operlog/remove',
		method: 'post',
		data: params
	});
}

/** 清空操作日志 */
export function cleanOperlog(params?: object){
	return request({
		url: system_api + '/operlog/clean',
		method: 'post',
		data: params
	});
}
