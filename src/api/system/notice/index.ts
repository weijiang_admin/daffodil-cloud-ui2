import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 获取通知管理列表
 * @param params 
 */
export function getNoticeList(params?: object){
	return request({
		url: system_api + '/notice/list',
		method: 'get',
		params: params
	})
}

/**
 * 获取通知信息
 * @param params 
 */
export function getNotice(params?: object){
	return request({
		url: system_api + '/notice/info',
		method: 'get',
		params: params
	})
}

/** 新增通知 */
export function addNotice(params?: object){
	return request({
		url: system_api + '/notice/add',
		method: 'post',
		data: params
	});
}

/** 修改通知 */
export function editNotice(params?: object) {
	return request({
		url: system_api + '/notice/edit',
		method: 'post',
		data: params
	});
}

/** 删除通知 */
export function delNotice(params?: object){
	return request({
		url: system_api + '/notice/remove',
		method: 'post',
		data: params
	});
}

/** 发布通知 */
export function pubNotice(params?: object) {
	return request({
		url: system_api + '/notice/pub',
		method: 'post',
		data: params
	});
}

