import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 获取群组管理列表
 * @param params 
 */
export function getGroupList(params?: object){
	return request({
		url: system_api + '/group/list',
		method: 'get',
		params: params
	})
}

/**
 * 获取群组信息
 * @param params 
 */
export function getGroup(params?: object){
	return request({
		url: system_api + '/group/info',
		method: 'get',
		params: params
	})
}

/** 新增群组 */
export function addGroup(params?: object){
	return request({
		url: system_api + '/group/add',
		method: 'post',
		data: params
	});
}

/** 修改群组 */
export function editGroup(params?: object) {
	return request({
		url: system_api + '/group/edit',
		method: 'post',
		data: params
	});
}

/** 删除群组 */
export function delGroup(params?: object){
	return request({
		url: system_api + '/group/remove',
		method: 'post',
		data: params
	});
}

/**
 * 获取群组用户管理列表
 * @param params 
 */
export function getUserGroupList(params?: object){
	return request({
		url: system_api + '/group/user/list',
		method: 'get',
		params: params
	})
}

/** 新增群组用户 */
export function addUserGroup(params?: object){
	return request({
		url: system_api + '/group/user/add',
		method: 'post',
		data: params
	});
}

/** 修改群组用户 */
export function editUserGroup(params?: object) {
	return request({
		url: system_api + '/group/user/edit',
		method: 'post',
		data: params
	});
}

/** 删除群组用户 */
export function delUserGroup(params?: object){
	return request({
		url: system_api + '/group/user/remove',
		method: 'post',
		data: params
	});
}

