import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 获取标签管理列表
 * @param params 
 */
export function getTagList(params?: object){
	return request({
		url: system_api + '/tag/list',
		method: 'get',
		params: params
	})
}

/**
 * 获取标签信息
 * @param params 
 */
export function getTag(params?: object){
	return request({
		url: system_api + '/tag/info',
		method: 'get',
		params: params
	})
}

/** 新增标签 */
export function addTag(params?: object){
	return request({
		url: system_api + '/tag/add',
		method: 'post',
		data: params
	});
}

/** 修改标签 */
export function editTag(params?: object) {
	return request({
		url: system_api + '/tag/edit',
		method: 'post',
		data: params
	});
}

/** 删除标签 */
export function delTag(params?: object){
	return request({
		url: system_api + '/tag/remove',
		method: 'post',
		data: params
	});
}

/**
 * 获取标签信息
 * @param params {tagLabel: 'user_info_tag'}
 */
export function getTagByLabel(params?: object){
	return request({
		url: system_api + '/tag/label',
		method: 'get',
		params: params
	})
}
