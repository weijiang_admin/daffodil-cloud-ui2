import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 获取岗位管理列表
 * @param params 
 */
export function getPostList(params?: object){
	return request({
		url: system_api + '/post/list',
		method: 'get',
		params: params
	})
}

/**
 * 获取岗位信息
 * @param params 
 */
export function getPost(params?: object){
	return request({
		url: system_api + '/post/info',
		method: 'get',
		params: params
	})
}

/** 新增岗位 */
export function addPost(params?: object){
	return request({
		url: system_api + '/post/add',
		method: 'post',
		data: params
	});
}

/** 修改岗位 */
export function editPost(params?: object) {
	return request({
		url: system_api + '/post/edit',
		method: 'post',
		data: params
	});
}

/** 删除岗位 */
export function delPost(params?: object){
	return request({
		url: system_api + '/post/remove',
		method: 'post',
		data: params
	});
}
