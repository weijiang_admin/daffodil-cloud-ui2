import request from '/@/utils/request';

const auth_api = import.meta.env.VITE_AUTH_API as string;

/**
 * 获取在线用户管理列表
 * @param params 
 */
export function getOnlineUserList(params?: object){
	return request({
		url: auth_api + '/onlineuser/list',
		method: 'get',
		params: params
	});
}

/**
 * 获取在线用户信息
 * @param params 
 */
export function getOnlineUser(params?: object){
	return request({
		url: auth_api + '/onlineuser/info',
		method: 'get',
		params: params
	});
}

/** 删除在线用户 */
export function delOnlineUser(params?: object){
	return request({
		url: auth_api + '/onlineuser/remove',
		method: 'post',
		data: params
	});
}

/** 清空在线用户 */
export function cleanOnlineUser(params?: object){
	return request({
		url: auth_api + '/onlineuser/clean',
		method: 'post',
		data: params
	});
}
