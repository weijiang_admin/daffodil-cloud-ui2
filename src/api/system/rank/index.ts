import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 获取职级管理列表
 * @param params 
 */
export function getRankList(params?: object){
	return request({
		url: system_api + '/rank/list',
		method: 'get',
		params: params
	})
}

/**
 * 获取职级信息
 * @param params 
 */
export function getRank(params?: object){
	return request({
		url: system_api + '/rank/info',
		method: 'get',
		params: params
	})
}

/** 新增职级 */
export function addRank(params?: object){
	return request({
		url: system_api + '/rank/add',
		method: 'post',
		data: params
	});
}

/** 修改职级 */
export function editRank(params?: object) {
	return request({
		url: system_api + '/rank/edit',
		method: 'post',
		data: params
	});
}

/** 删除职级 */
export function delRank(params?: object){
	return request({
		url: system_api + '/rank/remove',
		method: 'post',
		data: params
	});
}
