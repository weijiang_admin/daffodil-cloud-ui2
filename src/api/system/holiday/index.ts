import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 获取假期管理列表
 * @param params 
 */
export function getHolidayList(params?: object){
	return request({
		url: system_api + '/holiday/list',
		method: 'get',
		params: params
	})
}

/** 设置假期 */
export function setHoliday(params?: object) {
	return request({
		url: system_api + '/holiday/set',
		method: 'post',
		data: params
	});
}
