import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 获取登录日志管理列表
 * @param params 
 */
export function getLogininfoList(params?: object){
	return request({
		url: system_api + '/logininfo/list',
		method: 'get',
		params: params
	});
}

/**
 * 获取登录日志信息
 * @param params 
 */
export function getLogininfo(params?: object){
	return request({
		url: system_api + '/logininfo/info',
		method: 'get',
		params: params
	});
}

/** 删除登录日志 */
export function delLogininfo(params?: object){
	return request({
		url: system_api + '/logininfo/remove',
		method: 'post',
		data: params
	});
}

/** 清空登录日志 */
export function cleanLogininfo(params?: object){
	return request({
		url: system_api + '/logininfo/clean',
		method: 'post',
		data: params
	});
}
