import request from '/@/utils/request';

const sensitive_api = import.meta.env.VITE_SENSITIVE_API as string;

/**
 * 获取敏感词词语管理列表
 * @param params 
 */
export function getSensitiveWordsList(params?: object){
    return request({
        url: sensitive_api + '/words/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取敏感词词语
 * @param params 
 */
export function getSensitiveWords(params?: object){
    return request({
        url: sensitive_api + '/words/info',
        method: 'get',
        params: params
    })
}

/** 新增敏感词词语 */
export function addSensitiveWords(params?: object){
    return request({
        url: sensitive_api + '/words/add',
        method: 'post',
        data: params
    });
}

/** 修改敏感词词语 */
export function editSensitiveWords(params?: object) {
    return request({
        url: sensitive_api + '/words/edit',
        method: 'post',
        data: params
    });
}

/** 删除敏感词词语 */
export function delSensitiveWords(params?: object){
    return request({
        url: sensitive_api + '/words/remove',
        method: 'post',
        data: params
    });
}
