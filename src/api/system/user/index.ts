import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 获取用户管理列表
 * @param params 
 */
export function getUserList(params?: object){
	return request({
		url: system_api + '/user/list',
		method: 'get',
		params: params
	})
}

/**
 * 获取用户信息
 * @param params 
 */
export function getUser(params?: object){
	return request({
		url: system_api + '/user/info',
		method: 'get',
		params: params
	})
}

/** 新增用户 */
export function addUser(params?: object){
	return request({
		url: system_api + '/user/add',
		method: 'post',
		data: params
	});
}

/** 修改用户 */
export function editUser(params?: object) {
	return request({
		url: system_api + '/user/edit',
		method: 'post',
		data: params
	});
}

/** 删除用户 */
export function delUser(params?: object){
	return request({
		url: system_api + '/user/remove',
		method: 'post',
		data: params
	});
}

/** 密码重置 */
export function resetPwd(token: string, params?: object) {
	return request({
		url: system_api + '/user/resetPwd',
		method: 'post',
		headers: {
			'X-Request-Second-Verify-Token': token,
		},
		data: params
	});
}
