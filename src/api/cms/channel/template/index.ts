import request from '/@/utils/request';

const cms_api = import.meta.env.VITE_CMS_API as string;

/**
 * 获取栏目模板管理列表
 * @param params 
 */
export function getTemplateList(params?: object){
	return request({
		url: cms_api + '/template/list',
		method: 'get',
		params: params
	})
}

/**
 * 获取栏目模板信息
 * @param params 
 */
export function getTemplate(params?: object){
	return request({
		url: cms_api + '/template/info',
		method: 'get',
		params: params
	})
}

/** 新增栏目模板 */
export function addTemplate(params? : object){
	return request({
		url: cms_api + '/template/add',
		method: 'post',
		data: params
	});
}

/** 修改栏目模板 */
export function editTemplate(params?: object) {
	return request({
		url: cms_api + '/template/edit',
		method: 'post',
		data: params
	});
}

/** 删除栏目模板 */
export function delTemplate(params?: object){
	return request({
		url: cms_api + '/template/remove',
		method: 'post',
		data: params
	});
}

/** 发布栏目模板 */
export function releasedTemplate(params?: object){
	return request({
		url: cms_api + '/template/released/' + params.id,
		method: 'post'
	});
}

/** 撤销发布栏目模板 */
export function revokedTemplate(params?: object){
	return request({
		url: cms_api + '/template/revoked/' + params.id,
		method: 'post'
	});
}