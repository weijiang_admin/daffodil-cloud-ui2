import request from '/@/utils/request';

const cms_api = import.meta.env.VITE_CMS_API as string;

/**
 * 获取栏目管理列表
 * @param params 
 */
export function getChannelList(params?: object){
	return request({
		url: cms_api + '/channel/list',
		method: 'get',
		params: params
	})
}

/**
 * 获取栏目信息
 * @param params 
 */
export function getChannel(params?: object){
	return request({
		url: cms_api + '/channel/info',
		method: 'get',
		params: params
	})
}

/** 新增站点 */
export function addSite(params? : object){
	return request({
		url: cms_api + '/site/add',
		method: 'post',
		data: params
	});
}

/** 修改站点 */
export function editSite(params?: object) {
	return request({
		url: cms_api + '/site/edit',
		method: 'post',
		data: params
	});
}

/** 删除站点 */
export function delSite(params?: object){
	return request({
		url: cms_api + '/site/remove',
		method: 'post',
		data: params
	});
}

/** 获取配置 */
export function getSiteConfig(params?: object){
	return request({
		url: cms_api + '/site/config/info',
		method: 'get',
		params: params
	});
}

/** 站点配置 */
export function configSite(token: string, params?: object){
	return request({
		url: cms_api + '/site/config',
		method: 'post',
		headers: {
			'X-Request-Second-Verify-Token': token,
		},
		data: params
	});
}

/** 新增栏目 */
export function addChannel(params? : object){
	return request({
		url: cms_api + '/channel/add',
		method: 'post',
		data: params
	});
}

/** 修改栏目 */
export function editChannel(params?: object) {
	return request({
		url: cms_api + '/channel/edit',
		method: 'post',
		data: params
	});
}

/** 删除栏目 */
export function delChannel(params?: object){
	return request({
		url: cms_api + '/channel/remove',
		method: 'post',
		data: params
	});
}
