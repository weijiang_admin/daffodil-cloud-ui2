import request from '/@/utils/request';
import { Session } from '/@/utils/storage';
const server_url = import.meta.env.VITE_SERVER_URL as string;
const cms_api = import.meta.env.VITE_CMS_API as string;

/**
 * 获取稿件管理列表
 * @param params 
 */
export function getDocumentList(params?: object){
	return request({
		url: cms_api + '/document/list',
		method: 'get',
		params: params
	})
}

/**
 * 获取稿件信息
 * @param params 
 */
export function getDocument(params?: object){
	return request({
		url: cms_api + '/document/info',
		method: 'get',
		params: params
	})
}

/** 新增稿件 */
export function addDocument(params? : object){
	return request({
		url: cms_api + '/document/add',
		method: 'post',
		data: params
	});
}

/** 修改稿件 */
export function editDocument(params?: object) {
	return request({
		url: cms_api + '/document/edit',
		method: 'post',
		data: params
	});
}

/** 删除稿件 */
export function delDocument(params?: object){
	return request({
		url: cms_api + '/document/remove',
		method: 'post',
		data: params
	});
}

/** 移动稿件 */
export function moveDocument(params?: object){
	return request({
		url: cms_api + '/document/move',
		method: 'post',
		data: params
	});
}

/** 镜像引用稿件 */
export function mirrorDocument(params?: object){
	return request({
		url: cms_api + '/document/mirror',
		method: 'post',
		data: params
	});
}

/** 取消镜像引用稿件 */
export function unmirrorDocument(params?: object){
	return request({
		url: cms_api + '/document/unmirror',
		method: 'post',
		data: params
	});
}

/** 复制引用稿件 */
export function copyDocument(params?: object){
	return request({
		url: cms_api + '/document/copy',
		method: 'post',
		data: params
	});
}

/** 提审稿件 */
export function authDocument(params?: object){
	return request({
		url: cms_api + '/document/auth',
		method: 'post',
		data: params
	});
}

/** 通过提审稿件 */
export function allowedDocument(params?: object){
	return request({
		url: cms_api + '/document/allowed',
		method: 'post',
		data: params
	});
}

/** 未通过提审稿件 */
export function refusedDocument(params?: object){
	return request({
		url: cms_api + '/document/refused',
		method: 'post',
		data: params
	});
}

/** 发布稿件 */
export function releasedDocument(params?: object){
	return request({
		url: cms_api + '/document/released',
		method: 'post',
		data: params
	});
}

/** 定时发布稿件 */
export function timingDocument(params?: object){
	return request({
		url: cms_api + '/document/timing',
		method: 'post',
		data: params
	});
}

/** 撤销发布稿件 */
export function revokedDocument(params?: object){
	return request({
		url: cms_api + '/document/revoked',
		method: 'post',
		data: params
	});
}

/**
 * 获取稿件细览模板信息
 * @param params 
 */
export function getDocumentTemplate(params?: object){
	return request({
		url: cms_api + '/document/template/' + params.id,
		method: 'get',
		params: params
	})
}

/**
 * 预览稿件
 * @param params 
 */
export function previewDocument(params?: object){
	return `${server_url}${cms_api}/document/preview/${params.templateId}/${params.id}?access_token=${Session.get('token')}`;
}

/** 排序稿件 */
export function orderDocument(params?: object){
	return request({
		url: cms_api + '/document/order',
		method: 'post',
		data: params
	});
}