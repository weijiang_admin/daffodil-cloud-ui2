import request from '/@/utils/request';
import { Session } from '/@/utils/storage';
const server_url = import.meta.env.VITE_SERVER_URL as string;
const cms_api = import.meta.env.VITE_CMS_API as string;

/** 上传服务API */
export function uploadServer(){
	return `${server_url}${cms_api}/material/upload?accessToken=${Session.get('token')}`;
}

/** 上传稿件图片 */
export function uploadConfig(){
	return `${ uploadServer() }&action=config`;
}

/** 上传稿件图片 */
export function uploadPicture(){
	return `${ uploadServer() }&action=uploadimage`;
}

/** 分页查询素材资源 */
export function getMaterialList(params?: object){
	return request({
		url: cms_api + '/material/list',
		method: 'get',
		params: params
	})
}

/** 删除素材资源 */
export function delMaterial(params?: object){
	return request({
		url: cms_api + '/material/remove',
		method: 'post',
		data: params
	});
}