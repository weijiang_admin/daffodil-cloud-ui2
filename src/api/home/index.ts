import request from '/@/utils/request';

const system_api = import.meta.env.VITE_SYSTEM_API as string;

/**
 * 计数登录信息
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function countLoignInfo(params?: object){
	return request({
		url: system_api + '/home/login/count',
		method: 'get',
		params: params
	})
}

/**
 * 浏览器使用分布信息
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function browserLoignInfo(params?: object){
	return request({
		url: system_api + '/home/login/browser',
		method: 'get',
		params: params
	})
}

/**
 * 按月计数登录信息
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function countLoignInfoPerMonth(params?: object){
	return request({
		url: system_api + '/home/login/count/month',
		method: 'get',
		params: params
	})
}

/**
 * 计数操作信息
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function countOperLog(params?: object){
	return request({
		url: system_api + '/home/operlog/count',
		method: 'get',
		params: params
	})
}

/**
 * 按月计数操作信息
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function countOperLogPerMonth(params?: object){
	return request({
		url: system_api + '/home/operlog/count/month',
		method: 'get',
		params: params
	})
}

/**
 * 用户操作分布信息
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function businessLabelOperLog(params?: object){
	return request({
		url: system_api + '/home/operlog/business',
		method: 'get',
		params: params
	})
}

/**
 * 获取用户第三方应用
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function getUserThirdAppList(params?: object){
	return request({
		url: system_api + '/home/user/thirdapp',
		method: 'get',
		params: params
	})
}