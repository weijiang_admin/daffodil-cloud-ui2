import request from '/@/utils/request';

const crm_api = import.meta.env.VITE_CRM_API as string;

/**
 * 获取发票管理列表
 * @param params 
 */
export function getCrmInvoiceList(params?: object){
    return request({
        url: crm_api + '/invoice/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取发票
 * @param params 
 */
export function getCrmInvoice(params?: object){
    return request({
        url: crm_api + '/invoice/info',
        method: 'get',
        params: params
    })
}

/** 新增发票 */
export function addCrmInvoice(params? : object){
    return request({
        url: crm_api + '/invoice/add',
        method: 'post',
        data: params
    });
}

/** 修改发票 */
export function editCrmInvoice(params?: object) {
    return request({
        url: crm_api + '/invoice/edit',
        method: 'post',
        data: params
    });
}

/** 删除发票 */
export function delCrmInvoice(params?: object){
    return request({
        url: crm_api + '/invoice/remove',
        method: 'post',
        data: params
    });
}
/** 审核发票 */
export function examineCrmInvoice(params?: object){
    switch (params.status) {
        case '1':
            return request({
                url: crm_api + '/invoice/commited',
                method: 'post',
                data: params
            });
        case '2':
            return request({
                url: crm_api + '/invoice/approved',
                method: 'post',
                data: params
            });
        case '3':
            return request({
                url: crm_api + '/invoice/rejected',
                method: 'post',
                data: params
            });
        case '4':
            return request({
                url: crm_api + '/invoice/canceled',
                method: 'post',
                data: params
            });
        case '5':
            return request({
                url: crm_api + '/invoice/discard',
                method: 'post',
                data: params
            });
        default:
            break;
    }
}

export function changeCrmInvoice(params?: object){
    return request({
        url: crm_api + '/invoice/change',
        method: 'post',
        data: params
    });
}