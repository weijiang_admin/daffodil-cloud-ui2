import request from '/@/utils/request';

const crm_api = import.meta.env.VITE_CRM_API as string;

/**
 * @param params 
 */
export function countCrmContractByDay(params?: object){
    return request({
        url: crm_api + '/home/contract/count/day',
        method: 'get',
        params: params
    })
}

/**
 * @param params 
 */
export function countCrmCustomerByDay(params?: object){
    return request({
        url: crm_api + '/home/customer/count/day',
        method: 'get',
        params: params
    })
}

/**
 * @param params 
 */
export function countCrmEnterpriseByDay(params?: object){
    return request({
        url: crm_api + '/home/enterprise/count/day',
        method: 'get',
        params: params
    })
}

/**
 * @param params 
 */
export function countCrmProjectStatusByDay(params?: object){
    return request({
        url: crm_api + '/home/project/count/status',
        method: 'get',
        params: params
    })
}

/**
 * @param params 
 */
export function countCrmAmountByMonth(params?: object){
    return request({
        url: crm_api + '/home/amount/count/month',
        method: 'get',
        params: params
    })
}
