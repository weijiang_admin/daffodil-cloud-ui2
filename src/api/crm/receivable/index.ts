import request from '/@/utils/request';

const crm_api = import.meta.env.VITE_CRM_API as string;

/**
 * 获取回款信息管理列表
 * @param params 
 */
export function getCrmReceivableList(params?: object){
    return request({
        url: crm_api + '/receivable/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取回款信息
 * @param params 
 */
export function getCrmReceivable(params?: object){
    return request({
        url: crm_api + '/receivable/info',
        method: 'get',
        params: params
    })
}

/** 新增回款信息 */
export function addCrmReceivable(params? : object){
    return request({
        url: crm_api + '/receivable/add',
        method: 'post',
        data: params
    });
}

/** 修改回款信息 */
export function editCrmReceivable(params?: object) {
    return request({
        url: crm_api + '/receivable/edit',
        method: 'post',
        data: params
    });
}

/** 删除回款信息 */
export function delCrmReceivable(params?: object){
    return request({
        url: crm_api + '/receivable/remove',
        method: 'post',
        data: params
    });
}

/** 审核合同 */
export function examineCrmReceivable(params?: object){
    switch (params.status) {
        case '1':
            return request({
                url: crm_api + '/receivable/commited',
                method: 'post',
                data: params
            });
        case '2':
            return request({
                url: crm_api + '/receivable/approved',
                method: 'post',
                data: params
            });
        case '3':
            return request({
                url: crm_api + '/receivable/rejected',
                method: 'post',
                data: params
            });
        case '4':
            return request({
                url: crm_api + '/receivable/canceled',
                method: 'post',
                data: params
            });
        case '5':
            return request({
                url: crm_api + '/receivable/discard',
                method: 'post',
                data: params
            });
        default:
            break;
    }
}

export function changeCrmReceivable(params?: object){
    return request({
        url: crm_api + '/receivable/change',
        method: 'post',
        data: params
    });
}