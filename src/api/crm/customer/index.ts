import request from '/@/utils/request';

const crm_api = import.meta.env.VITE_CRM_API as string;

/**
 * 获取客户管理列表
 * @param params 
 */
export function getCrmCustomerList(params?: object){
    return request({
        url: crm_api + '/customer/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取客户
 * @param params 
 */
export function getCrmCustomer(params?: object){
    return request({
        url: crm_api + '/customer/info',
        method: 'get',
        params: params
    })
}

/** 新增客户 */
export function addCrmCustomer(params? : object){
    return request({
        url: crm_api + '/customer/add',
        method: 'post',
        data: params
    });
}

/** 修改客户 */
export function editCrmCustomer(params?: object) {
    return request({
        url: crm_api + '/customer/edit',
        method: 'post',
        data: params
    });
}

/** 删除客户 */
export function delCrmCustomer(params?: object){
    return request({
        url: crm_api + '/customer/remove',
        method: 'post',
        data: params
    });
}


export function changeCrmCustomer(params?: object){
    return request({
        url: crm_api + '/customer/change',
        method: 'post',
        data: params
    });
}
