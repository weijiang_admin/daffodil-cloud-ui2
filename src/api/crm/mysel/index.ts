import request from '/@/utils/request';

const crm_api = import.meta.env.VITE_CRM_API as string;

/**
 * 分页查询用户合同列表
 * @param params 
 */
export function getMyselContractList(params?: object){
    return request({
        url: crm_api + '/mysel/contract/list',
        method: 'get',
        params: params
    })
}

/**
 * 分页查询用户项目列表
 * @param params 
 */
export function getMyselProjectList(params?: object){
    return request({
        url: crm_api + '/mysel/project/list',
        method: 'get',
        params: params
    })
}

/**
 * 分页查询用户项目过程列表
 * @param params 
 */
export function getMyselProjectProcessList(params?: object){
    return request({
        url: crm_api + '/mysel/project/process/list',
        method: 'get',
        params: params
    })
}