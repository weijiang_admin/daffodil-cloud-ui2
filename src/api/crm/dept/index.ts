import request from '/@/utils/request';

const crm_api = import.meta.env.VITE_CRM_API as string;

/**
 * 获取部门信息管理列表
 * @param params 
 */
export function getCrmDeptList(params?: object){
    return request({
        url: crm_api + '/dept/list',
        method: 'get',
        params: params
    })
}