import request from '/@/utils/request';

const crm_api = import.meta.env.VITE_CRM_API as string;

/**
 * 获取租借管理列表
 * @param params 
 */
export function getCrmRentalList(params?: object){
    return request({
        url: crm_api + '/rental/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取租借
 * @param params 
 */
export function getCrmRental(params?: object){
    return request({
        url: crm_api + '/rental/info',
        method: 'get',
        params: params
    })
}

/** 新增租借 */
export function addCrmRental(params? : object){
    return request({
        url: crm_api + '/rental/add',
        method: 'post',
        data: params
    });
}

/** 修改租借 */
export function editCrmRental(params?: object) {
    return request({
        url: crm_api + '/rental/edit',
        method: 'post',
        data: params
    });
}

/** 删除租借 */
export function delCrmRental(params?: object){
    return request({
        url: crm_api + '/rental/remove',
        method: 'post',
        data: params
    });
}

/** 续期租借 */
export function renewalCrmRental(params?: object){
    return request({
        url: crm_api + '/rental/renewal',
        method: 'post',
        data: params
    });
}

/** 终止租借 */
export function finishedCrmRental(params?: object){
    return request({
        url: crm_api + '/rental/finished',
        method: 'post',
        data: params
    });
}
