import request from '/@/utils/request';

const crm_api = import.meta.env.VITE_CRM_API as string;

/**
 * 获取用户信息管理列表
 * @param params 
 */
export function getCrmUserList(params?: object){
    return request({
        url: crm_api + '/user/list',
        method: 'get',
        params: params
    })
}