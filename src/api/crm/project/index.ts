import request from '/@/utils/request';

const crm_api = import.meta.env.VITE_CRM_API as string;

/**
 * 获取项目管理列表
 * @param params 
 */
export function getCrmProjectList(params?: object){
    return request({
        url: crm_api + '/project/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取项目
 * @param params 
 */
export function getCrmProject(params?: object){
    return request({
        url: crm_api + '/project/info',
        method: 'get',
        params: params
    })
}

/** 新增项目 */
export function addCrmProject(params? : object){
    return request({
        url: crm_api + '/project/add',
        method: 'post',
        data: params
    });
}

/** 修改项目 */
export function editCrmProject(params?: object) {
    return request({
        url: crm_api + '/project/edit',
        method: 'post',
        data: params
    });
}

/** 删除项目 */
export function delCrmProject(params?: object){
    return request({
        url: crm_api + '/project/remove',
        method: 'post',
        data: params
    });
}

/** 状态项目 */
export function editCrmProjectStatus(params?: object){
    switch (params.status) {
        case '1':
            return request({
                url: crm_api + '/project/activing/' + params.id,
                method: 'post',
                data: params
            });
        case '3':
            return request({
                url: crm_api + '/project/paused/' + params.id,
                method: 'post',
                data: params
            });
        case '2':
            return request({
                url: crm_api + '/project/finished/' + params.id,
                method: 'post',
                data: params
            });
        case '4':
            return request({
                url: crm_api + '/project/stopped/' + params.id,
                method: 'post',
                data: params
            });
        default:
            break;
    }
}

export function changeCrmProject(params?: object){
    return request({
        url: crm_api + '/project/change',
        method: 'post',
        data: params
    });
}

export function finishCrmProject(params?: object){
    return request({
        url: crm_api + '/project/finished',
        method: 'post',
        data: params
    });
}
export function amountCrmProject(params?: object){
    return request({
        url: crm_api + '/project/amount',
        method: 'get',
        params: params
    })
}