import request from '/@/utils/request';

const crm_api = import.meta.env.VITE_CRM_API as string;

/**
 * 获取项目业务分类管理列表
 * @param params 
 */
export function getCrmProjectCategoryList(params?: object){
    return request({
        url: crm_api + '/project/category/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取项目业务分类
 * @param params 
 */
export function getCrmProjectCategory(params?: object){
    return request({
        url: crm_api + '/project/category/info',
        method: 'get',
        params: params
    })
}

/** 新增项目业务分类 */
export function addCrmProjectCategory(params? : object){
    return request({
        url: crm_api + '/project/category/add',
        method: 'post',
        data: params
    });
}

/** 修改项目业务分类 */
export function editCrmProjectCategory(params?: object) {
    return request({
        url: crm_api + '/project/category/edit',
        method: 'post',
        data: params
    });
}

/** 删除项目业务分类 */
export function delCrmProjectCategory(params?: object){
    return request({
        url: crm_api + '/project/category/remove',
        method: 'post',
        data: params
    });
}
