import request from '/@/utils/request';

const crm_api = import.meta.env.VITE_CRM_API as string;

/**
 * 获取过程管理列表
 * @param params 
 */
export function getCrmProjectProcessList(params?: object){
    return request({
        url: crm_api + '/project/process/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取过程
 * @param params 
 */
export function getCrmProjectProcess(params?: object){
    return request({
        url: crm_api + '/project/process/info',
        method: 'get',
        params: params
    })
}

/** 新增过程 */
export function addCrmProjectProcess(params? : object){
    return request({
        url: crm_api + '/project/process/add',
        method: 'post',
        data: params
    });
}

/** 修改过程 */
export function editCrmProjectProcess(params?: object) {
    return request({
        url: crm_api + '/project/process/edit',
        method: 'post',
        data: params
    });
}

export function assignCrmProjectProcess(params?: object) {
    return request({
        url: crm_api + '/project/process/assign',
        method: 'post',
        data: params
    });
}

/** 删除过程 */
export function delCrmProjectProcess(params?: object){
    return request({
        url: crm_api + '/project/process/remove',
        method: 'post',
        data: params
    });
}

/** 项目过程状态 */
export function editCrmProjectProcessStatus(params?: object){
    switch (params.status) {
        case '1':
            return request({
                url: crm_api + '/project/process/activing/' + params.id,
                method: 'post',
                data: params
            });
        case '3':
            return request({
                url: crm_api + '/project/process/paused/' + params.id,
                method: 'post',
                data: params
            });
        case '2':
            return request({
                url: crm_api + '/project/process/finished/' + params.id,
                method: 'post',
                data: params
            });
        default:
            break;
    }
}