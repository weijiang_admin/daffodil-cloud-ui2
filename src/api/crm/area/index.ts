import request from '/@/utils/request';

const crm_api = import.meta.env.VITE_CRM_API as string;

/**
 * 获取区划信息管理列表
 * @param params 
 */
export function getCrmAreaList(params?: object){
    return request({
        url: crm_api + '/area/list',
        method: 'get',
        params: params
    })
}