import request from '/@/utils/request';

const crm_api = import.meta.env.VITE_CRM_API as string;

/**
 * 获取企业管理列表
 * @param params 
 */
export function getCrmEnterpriseList(params?: object){
    return request({
        url: crm_api + '/enterprise/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取企业
 * @param params 
 */
export function getCrmEnterprise(params?: object){
    return request({
        url: crm_api + '/enterprise/info',
        method: 'get',
        params: params
    })
}

/** 新增企业 */
export function addCrmEnterprise(params? : object){
    return request({
        url: crm_api + '/enterprise/add',
        method: 'post',
        data: params
    });
}

/** 修改企业 */
export function editCrmEnterprise(params?: object) {
    return request({
        url: crm_api + '/enterprise/edit',
        method: 'post',
        data: params
    });
}

/** 删除企业 */
export function delCrmEnterprise(params?: object){
    return request({
        url: crm_api + '/enterprise/remove',
        method: 'post',
        data: params
    });
}

/** 变更企业 */
export function changeCrmEnterprise(params?: object){
    return request({
        url: crm_api + '/enterprise/change',
        method: 'post',
        data: params
    });
}
