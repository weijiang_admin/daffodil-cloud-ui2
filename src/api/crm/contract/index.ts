import request from '/@/utils/request';

const crm_api = import.meta.env.VITE_CRM_API as string;

/**
 * 获取合同管理列表
 * @param params 
 */
export function getCrmContractList(params?: object){
    return request({
        url: crm_api + '/contract/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取合同
 * @param params 
 */
export function getCrmContract(params?: object){
    return request({
        url: crm_api + '/contract/info',
        method: 'get',
        params: params
    })
}

/** 新增合同 */
export function addCrmContract(params? : object){
    return request({
        url: crm_api + '/contract/add',
        method: 'post',
        data: params
    });
}

/** 修改合同 */
export function editCrmContract(params?: object) {
    return request({
        url: crm_api + '/contract/edit',
        method: 'post',
        data: params
    });
}

/** 删除合同 */
export function delCrmContract(params?: object){
    return request({
        url: crm_api + '/contract/remove',
        method: 'post',
        data: params
    });
}

/** 审核合同 */
export function examineCrmContract(params?: object){
    switch (params.status) {
        case '1':
            return request({
                url: crm_api + '/contract/commited',
                method: 'post',
                data: params
            });
        case '2':
            return request({
                url: crm_api + '/contract/approved',
                method: 'post',
                data: params
            });
        case '3':
            return request({
                url: crm_api + '/contract/rejected',
                method: 'post',
                data: params
            });
        case '4':
            return request({
                url: crm_api + '/contract/canceled',
                method: 'post',
                data: params
            });
        case '5':
            return request({
                url: crm_api + '/contract/discard',
                method: 'post',
                data: params
            });
        default:
            break;
    }
}

//终止合同
export function finishedCrmContract(params?: object){
    return request({
        url: crm_api + '/contract/finished',
        method: 'post',
        data: params
    });
}

//续期合同
export function renewalCrmContract(params?: object){
    return request({
        url: crm_api + '/contract/renewal',
        method: 'post',
        data: params
    });
}

//归属合同
export function changeCrmContract(params?: object){
    return request({
        url: crm_api + '/contract/change',
        method: 'post',
        data: params
    });
}
//交付合同
export function deliveryCrmContract(params?: object){
    return request({
        url: crm_api + '/contract/delivery',
        method: 'post',
        data: params
    });
}
//取消交付
export function undeliveryCrmContract(params?: object){
    return request({
        url: crm_api + '/contract/undelivery',
        method: 'post',
        data: params
    })
}
//合计金额
export function amountCrmContract(params?: object){
    return request({
        url: crm_api + '/contract/amount',
        method: 'get',
        params: params
    })
}