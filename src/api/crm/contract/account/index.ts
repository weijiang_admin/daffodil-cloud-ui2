import request from '/@/utils/request';

const crm_api = import.meta.env.VITE_CRM_API as string;

/**
 * 获取记账管理列表
 * @param params 
 */
export function getCrmContractAccountList(params?: object){
    return request({
        url: crm_api + '/contract/account/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取记账
 * @param params 
 */
export function getCrmContractAccount(params?: object){
    return request({
        url: crm_api + '/contract/account/info',
        method: 'get',
        params: params
    })
}

/** 新增记账 */
export function addCrmContractAccount(params? : object){
    return request({
        url: crm_api + '/contract/account/add',
        method: 'post',
        data: params
    });
}

/** 修改记账 */
export function editCrmContractAccount(params?: object) {
    return request({
        url: crm_api + '/contract/account/edit',
        method: 'post',
        data: params
    });
}

/** 删除记账 */
export function delCrmContractAccount(params?: object){
    return request({
        url: crm_api + '/contract/account/remove',
        method: 'post',
        data: params
    });
}
