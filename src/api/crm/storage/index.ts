import request from '/@/utils/request';

const crm_api = import.meta.env.VITE_CRM_API as string;

/** 上传对象
 * @param key 文件夹名称
 * @param params source 远端网络地址
 */
export function uploadStorage(params?: object){
	return crm_api + '/storage/upload';
}