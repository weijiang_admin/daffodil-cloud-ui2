import request from '/@/utils/request';

const crm_api = import.meta.env.VITE_CRM_API as string;

/**
 * 获取人才管理列表
 * @param params 
 */
export function getCrmTalenterList(params?: object){
    return request({
        url: crm_api + '/talenter/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取人才
 * @param params 
 */
export function getCrmTalenter(params?: object){
    return request({
        url: crm_api + '/talenter/info',
        method: 'get',
        params: params
    })
}

/** 新增人才 */
export function addCrmTalenter(params? : object){
    return request({
        url: crm_api + '/talenter/add',
        method: 'post',
        data: params
    });
}

/** 修改人才 */
export function editCrmTalenter(params?: object) {
    return request({
        url: crm_api + '/talenter/edit',
        method: 'post',
        data: params
    });
}

/** 删除人才 */
export function delCrmTalenter(params?: object){
    return request({
        url: crm_api + '/talenter/remove',
        method: 'post',
        data: params
    });
}

/** 审核人才 */
export function examineCrmTalenter(params?: object){
    switch (params.status) {
        case '1':
            return request({
                url: crm_api + '/talenter/commited',
                method: 'post',
                data: params
            });
        case '2':
            return request({
                url: crm_api + '/talenter/approved',
                method: 'post',
                data: params
            });
        case '3':
            return request({
                url: crm_api + '/talenter/rejected',
                method: 'post',
                data: params
            });
        case '4':
            return request({
                url: crm_api + '/talenter/canceled',
                method: 'post',
                data: params
            });
        case '5':
            return request({
                url: crm_api + '/talenter/discard',
                method: 'post',
                data: params
            });
        default:
            break;
    }
}

export function changeCrmTalenter(params?: object){
    return request({
        url: crm_api + '/talenter/change',
        method: 'post',
        data: params
    });
}

export function renewalCrmTalenter(params?: object){
    return request({
        url: crm_api + '/talenter/renewal',
        method: 'post',
        data: params
    });
}

export function finishedCrmTalenter(params?: object){
    return request({
        url: crm_api + '/talenter/finished',
        method: 'post',
        data: params
    })
}