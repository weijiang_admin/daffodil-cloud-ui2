import request from '/@/utils/request';

const crm_api = import.meta.env.VITE_CRM_API as string;

/**
 * 获取支出管理列表
 * @param params 
 */
export function getCrmExpenseList(params?: object){
    return request({
        url: crm_api + '/expense/list',
        method: 'get',
        params: params
    })
}

/**
 * 获取支出
 * @param params 
 */
export function getCrmExpense(params?: object){
    return request({
        url: crm_api + '/expense/info',
        method: 'get',
        params: params
    })
}

/** 新增支出 */
export function addCrmExpense(params? : object){
    return request({
        url: crm_api + '/expense/add',
        method: 'post',
        data: params
    });
}

/** 修改支出 */
export function editCrmExpense(params?: object) {
    return request({
        url: crm_api + '/expense/edit',
        method: 'post',
        data: params
    });
}

/** 删除支出 */
export function delCrmExpense(params?: object){
    return request({
        url: crm_api + '/expense/remove',
        method: 'post',
        data: params
    });
}

/** 审核合同 */
export function examineCrmExpense(params?: object){
    switch (params.status) {
        case '1':
            return request({
                url: crm_api + '/expense/commited',
                method: 'post',
                data: params
            });
        case '2':
            return request({
                url: crm_api + '/expense/approved',
                method: 'post',
                data: params
            });
        case '3':
            return request({
                url: crm_api + '/expense/rejected',
                method: 'post',
                data: params
            });
        case '4':
            return request({
                url: crm_api + '/expense/canceled',
                method: 'post',
                data: params
            });
        case '5':
            return request({
                url: crm_api + '/expense/discard',
                method: 'post',
                data: params
            });
        default:
            break;
    }
}

/** 变更支出 */
export function changeCrmExpense(params?: object){
    return request({
        url: crm_api + '/expense/change',
        method: 'post',
        data: params
    });
}