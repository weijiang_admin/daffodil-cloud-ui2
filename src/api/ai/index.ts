import request from '/@/utils/request';

const aimodel_api = '/api-ai';

/**
 * 获取星火模型AI参数
 * @param params 
 */
export function getSparkChatInfo(params?: object){
	return request({
		url: aimodel_api + '/spark/chat/info',
		method: 'get',
		params: params
	})
}