import request from '/@/utils/request';

const monitor_api = import.meta.env.VITE_MONITOR_API as string;

/**
 * 获取Redis缓存健康信息数据
 * @param params 
 */
export function getRedisCacheHealth(params?: object){
	return request({
		url: monitor_api + '/rediscache/health',
		method: 'get',
		params: params
	})
}
