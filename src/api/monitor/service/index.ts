import request from '/@/utils/request';
import { Session } from '/@/utils/storage';
import { Sign } from '/@/utils/sign';

const server_url = import.meta.env.VITE_SERVER_URL as string;
const monitor_api = import.meta.env.VITE_MONITOR_API as string;

/**
 * 获取Nacos服务实例列表
 * @param params 
 */
export function getServiceList(params?: object){
	return request({
		url: monitor_api + '/service/list',
		method: 'get',
		params: params
	})
}

/**
 * 获取服务主机磁盘情况
 * @param params 
 */
export function getServiceDisk(params?: object){
	return request({
		url: monitor_api + '/service/disk',
		method: 'get',
		params: params
	});
}

/**
 * 获取实例服务应用信息
 * @param params 
 */
export function getServiceAppInfo(params?: object){
	return request({
		url: monitor_api + '/service/appinfo',
		method: 'get',
		params: params
	});
}

/**
 * 获取实例服务JVM信息
 * @param params 
 */
export function getServiceJvmInfo(params?: object){
	return request({
		url: monitor_api + '/service/jvminfo',
		method: 'get',
		params: params
	});
}

/**
 * 获取服务主机CPU情况
 * @param params 
 */
export function getServiceCpu(params?: any){
	const { signature, secret, timestamp } = Sign.signature({
		url: `${monitor_api}/service/cpu`,
		params,
	});
	return new EventSource(`${server_url}${monitor_api}/service/cpu?hashKey=${params.hashKey}&accessToken=${Session.get('token')}`);
}

/**
 * 获取实例服务JVM内存情况
 * @param params 
 */
export function getServiceMemory(params?: any){
	return new EventSource(`${server_url}${monitor_api}/service/memory?hashKey=${params.hashKey}&accessToken=${Session.get('token')}`);
}

/**
 * 获取实例服务进程信息
 * @param params 
 */
export function getServiceThreadInfo(params?: any){
	return new EventSource(`${server_url}${monitor_api}/service/thread?hashKey=${params.hashKey}&accessToken=${Session.get('token')}`);
}

